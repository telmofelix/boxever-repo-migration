
import yaml
import csv


def persist_job_details():
    with open('../data/jenkins-vars.yml', mode='r') as yml_file:
        document = yaml.full_load(yml_file)

        with open('../data/ansible_groups.csv', mode='w') as csv_file:
            writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(['jenkins_group', 'scm_slug'])

            for jenkins_group in document.items():
                for repo in jenkins_group[1]:
                    writer.writerow([jenkins_group[0], repo])


class BoxeverAnsible(object):
    def __init__(self, server):
        self.server = server



