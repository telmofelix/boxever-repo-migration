import jenkins


from pybitbucket.auth import Anonymous, BasicAuthenticator, OAuth1Authenticator
from pybitbucket.repository import (
    RepositoryRole, RepositoryType, RepositoryForkPolicy,
    Repository, RepositoryV1,
    RepositoryPayload)

from analyzer.bx_bitbucket import BoxeverBitbucket, get_bitbucket_server_instance
from analyzer.bx_jenkins import BoxeverJenkins

import secrets

import json
import pickle
import logging

from analyzer.secrets import boxever_email, jenkins_token, jenkins_poc_token, bitbucket_client_key, bitbucket_client_secret


def get_jenkins_server_instance():
    jenkins_url = "http://localhost:3006"
    return jenkins.Jenkins(jenkins_url, username=boxever_email, password=jenkins_token)

def get_jenkins_poc_server_instance():
    jenkins_url = "https://jenkins2poc.boxever.com/api/json?pretty=true"
    return jenkins.Jenkins(jenkins_url, username=boxever_email,
                           password=jenkins_poc_token)



def fetch_jenkins_data(bx_jenkins):
    print(bx_jenkins.persist_job_details())

def fetch_bitbucket_data(bx_jenkins):
    bx_bitbucket.persist_repo_details()

if __name__ == '__main__':
    logging.basicConfig(filename='analyzer.log', level=logging.INFO)
    FETCH_JENKINS_DATA = False
    FETCH_BITBUCKET_DATA = False

    bx_jenkins = BoxeverJenkins(get_jenkins_server_instance())
    if FETCH_JENKINS_DATA:
        fetch_jenkins_data(bx_jenkins)

    bx_bitbucket = BoxeverBitbucket(get_bitbucket_server_instance())
    if FETCH_BITBUCKET_DATA:
        fetch_bitbucket_data(bx_bitbucket)

    # produce only jenkins slugs
    # job_slugs = bx_jenkins.load_job_details()
    # bx_bitbucket.output_csv(job_slugs)

    # produce all slugs
    bx_bitbucket.output_csv(bx_bitbucket.get_all_slugs())