import glob
import json

import jenkins
import os
import xmltodict
import re
import logging
import csv


def get_scm(job_name, scm_config_dict, type):
    if type == "hudson.plugins.git.GitSCM":
        scm_url = scm_config_dict['userRemoteConfigs']['hudson.plugins.git.UserRemoteConfig']['url']
        logging.debug("%s %s" % (job_name, scm_url))
    elif type == "hudson.plugins.mercurial.MercurialSCM":
        scm_url = scm_config_dict['source']
        logging.debug("%s %s" % (job_name, scm_url))
    # elif type == "hudson.scm.NullSCM":
    #     print("NULL scm")
    else:
        scm_url = ''
        logging.debug("UKN scm")
    return scm_url


def parse_job_config(job_name, config_dict):
    project_types = config_dict.keys()
    if 'maven2-moduleset' in list(config_dict):
        scm = get_scm(job_name, config_dict['maven2-moduleset']['scm'],
                      config_dict['maven2-moduleset']['scm']['@class'])
    elif 'project' in list(config_dict):
        scm = get_scm(job_name, config_dict['project']['scm'], config_dict['project']['scm']['@class'])
    else:
        logging.debug('Job unknown: %s' % project_types)
        scm = ''
    return scm


def extract_repo_slug(scm_url):
    try:
        repo_slug = re.search('(.+?)boxever/(.+?)$', scm_url).group(2)
        if(repo_slug.endswith('.git')):
            repo_slug = repo_slug[0:len(repo_slug)-4]
    except AttributeError:
        repo_slug = ''
    return repo_slug


class BoxeverJenkins(object):
    def __init__(self, server):
        self.server = server

    def get_jobs_count(self):
        print(self.server.jobs_count())

    def persist_job_details(self, output_csv=True):
        server_jobs = self.server.get_jobs() #[0:19]
        csv_records = []

        for job in server_jobs:
            job_config = xmltodict.parse(self.server.get_job_config(job["name"]))
            job_info = self.server.get_job_info(job["name"])

            logging.info('Job Name: %s Job info: %s' % (job["name"], job_info["color"]))

            job_record = {'job_name': job["name"], 'job_config': job_config, 'job_info': job_info}
            if output_csv:
                csv_records.append(job_record)

            json.dump(job_record, open("../data/jenkins/" + job["name"] + ".json", 'w'))

        if output_csv:
            self.persist_csv(csv_records)

    @staticmethod
    def persist_csv(csv_records):
        with open('../data/jenkins.csv', mode='w') as csv_file:
            writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(['job_name', 'scm_url', 'scm_slug', 'url', 'color'])
            for job_record in csv_records:
                job_name = job_record['job_name']
                scm_url = parse_job_config(job_name, job_record['job_config'])
                scm_slug = extract_repo_slug(scm_url)
                url = job_record['job_info']['url']
                color = job_record['job_info']['color']

                writer.writerow([job_name, scm_url, scm_slug, url, color])

    @staticmethod
    def load_job_details():
        job_slugs = set()
        jenkin_jobs = glob.glob("../data/jenkins/*.json")
        for job in jenkin_jobs:
            with open(job, 'r') as json_file:
                job_record = json.load(json_file)

                job_name = job_record['job_name']
                scm_url = parse_job_config(job_name, job_record['job_config'])
                scm_slug = extract_repo_slug(scm_url)

                job_slugs.add(scm_slug)

                logging.info("url: %s %s" % (scm_url, scm_slug))

        job_slugs.remove('')
        logging.info("job_slugs: %s" % (job_slugs))
        return job_slugs
