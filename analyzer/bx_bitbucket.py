import glob
import json

import jenkins
import os
import xmltodict
import pickle
import csv
import logging

from pybitbucket.repository import (
    RepositoryRole, RepositoryType, RepositoryForkPolicy,
    Repository, RepositoryV1,
    RepositoryPayload)

from pybitbucket.auth import Anonymous, BasicAuthenticator, OAuth1Authenticator
from pybitbucket.bitbucket import BitbucketBase, Client, PayloadBuilder

from analyzer.secrets import bitbucket_client_key, bitbucket_client_secret

def get_bitbucket_server_instance():
    bitbucket_client = Client(
        OAuth1Authenticator(
            bitbucket_client_key,
            bitbucket_client_secret))
    return bitbucket_client

class BoxeverBitbucket(object):
    def __init__(self,  bitbucket_client):
        self. bitbucket_client =  bitbucket_client

    def repos_by_owner_and_role(self, role):
        return Repository.find_repositories_by_owner_and_role(owner="Boxever", role=role, client=self.bitbucket_client)

    def repo_by_full_name(self, full_name):
        return Repository.find_repository_by_full_name(full_name, client=self.bitbucket_client)

    def persist_repo_details(self):
        role="member"
        # role = "admin"
        for repo in self.repos_by_owner_and_role(role):
            logging.debug("persisting " + repo.slug + " " + repo.scm)
            pickle.dump(repo, open("../data/bitbucket/" + repo.slug + ".pickle", 'wb'), protocol=0)

    @staticmethod
    def load_repo_detail(repo_slug):
        fd = open("../data/bitbucket/" + repo_slug + ".pickle", "rb")
        repo = pickle.load(fd)
        logging.debug("loading from pickle " + repo.slug + " " + repo.scm + " " + repo.full_name)
        fd.close()
        return repo

    @staticmethod
    def get_all_slugs():
        scm_slugs = set()
        bitbucket_repos = glob.glob("../data/bitbucket/*.pickle")
        for repo in bitbucket_repos:
            # remove the ../data.. and the .pickle at the end of the string
            scm_slug = repo[18:len(repo)-7]
            scm_slugs.add(scm_slug)
        return scm_slugs

    def output_csv(self, job_slugs):
        with open('../data/bitbucket.csv', mode='w') as csv_file:
            writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(['scm', 'slug', 'full_name', 'created_on_date', 'updated_on_date', 'size', 'project_key', 'project_name'])
            for slug in job_slugs:
                logging.debug("slug: %s" % slug)
                try:
                    repo = self.load_repo_detail(repo_slug=slug)
                    writer.writerow([repo.scm, repo.slug, repo.full_name, repo.created_on[:10], repo.updated_on[:10], repo.size, repo.project['key'], repo.project['name']])
                except FileNotFoundError:
                    logging.warning("repo not found! slug=" + slug)
