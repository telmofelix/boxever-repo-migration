usage() {
  echo "fix-revision.sh -r <REVISION>";
  echo "Revision number is a required parameter. Use only the digits, do not add the 'r' to it"
  echo "Remember this must be run from the repos/hg/<mercurial-repo>/ directory"
  echo "Example: fix-revision.sh -r 10"

  exit 1
}

if [ $# -ne 2 ]; then
    usage
fi

REVISION=$2

hg update -r "${REVISION}"
hg branch "r${REVISION}-fix-git-conversion"
hg commit -m "Fix git conversion (unnamed head r${REVISION})"
hg update default