#!/bin/bash






function download_repo() {
    # clone repo
  if [ -d "hg/$1.hg" ]
  then
    echo "--- $1.hg exists: updating"
    pushd hg/"$1".hg || return
    hg pull -u ssh://hg@bitbucket.org/boxever/"$1" >> ../../logs/update."$1".log 2>&1
    popd || return
  else
    echo "--- $1.hg does not exist: cloning"
    hg clone --noupdate ssh://hg@bitbucket.org/boxever/"$1" hg/"$1".hg >> logs/update."$1".log 2>&1
  fi
}


#REPO_NAME="scrapbook"
#migrate_repo ${REPO_NAME}

while read REPO; do
  echo "-- preparing to download ${REPO}"
  (download_repo ${REPO}) &
done < slugs.txt
wait

#export PS1='(repo-migration) \T \u@\h \n \[\033[1;33m\]\w\[\033[0m\]$(parse_git_branch)$(parse_hg_branch)$'