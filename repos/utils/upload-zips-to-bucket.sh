S3_BUCKET="s3://boxever-bitbucket-backups-dev-eu-west-1/archived/2020-03-24/"
for ZIP in *.zip; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done


S3_BUCKET="s3://boxever-bitbucket-backups-dev-eu-west-1/backup/2020-03-29/hg/"
for ZIP in *.zip; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done


# backup for the batches of gits
export S3_BUCKET="s3://boxever-bitbucket-backups-dev-eu-west-1/backup/2020-03-31/git/part-00/"
for ZIP in *.zip; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done
for ZIP in *.log; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done

export S3_BUCKET="s3://boxever-bitbucket-backups-dev-eu-west-1/backup/2020-03-31/git/part-01/"
for ZIP in *.zip; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done
for ZIP in *.log; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done

export S3_BUCKET="s3://boxever-bitbucket-backups-dev-eu-west-1/backup/2020-03-31/git/part-02/"
for ZIP in *.zip; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done
for ZIP in *.log; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done

export S3_BUCKET="s3://boxever-bitbucket-backups-dev-eu-west-1/backup/2020-03-31/git/part-03/"
for ZIP in *.zip; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done
for ZIP in *.log; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done

export S3_BUCKET="s3://boxever-bitbucket-backups-dev-eu-west-1/backup/2020-03-31/git/part-04/"
for ZIP in *.zip; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done
for ZIP in *.log; do aws s3 cp "${ZIP}" "${S3_BUCKET}"; done