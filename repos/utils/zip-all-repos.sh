# remove echo from command; run from hg directory to create bundle of zips
for REPO in *.hg; do echo zip -r "${REPO}.zip" "$REPO/"; done

# remove echo from command; run from git directory to create bundle of zips
for REPO in *.git; do echo zip -r "${REPO}.zip" "$REPO/"; done