#!/bin/bash






function migrate_repo() {
  pushd git

  # init empty git repo
  rm -rf "$1".git
  mkdir "$1".git
  git init "$1".git/

  # export repo
  cd "$1".git || exit
  git config core.ignoreCase false

  ~/github/fast-export/hg-fast-export.sh -r ../../hg/"$1".hg >> ../../logs/migration."$1".log 2>&1

  git config core.ignoreCase true
  cd ..

  popd
}


#REPO_NAME="scrapbook"
#migrate_repo ${REPO_NAME}

while read REPO; do
  echo "---- preparing to migrate ${REPO}"
  (migrate_repo "${REPO}") &
done < slugs.txt
wait

#export PS1='(repo-migration) \T \u@\h \n \[\033[1;33m\]\w\[\033[0m\]$(parse_git_branch)$(parse_hg_branch)$'