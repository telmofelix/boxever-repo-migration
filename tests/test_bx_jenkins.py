import unittest
from unittest import TestCase

from analyzer.bx_jenkins import extract_repo_slug


class TestBoxeverJenkins(TestCase):
    def test_extract_repo_slug(self):
        scm_url = 'somebadstring'
        self.assertEqual('', extract_repo_slug(scm_url))

        scm_url = 'git@bitbucket.org:boxever/boxever-guest-context-job-scala.git'
        self.assertEqual('boxever-guest-context-job-scala', extract_repo_slug(scm_url))

        scm_url = 'ssh://hg@bitbucket.org/boxever/boxever-flows-service'
        self.assertEqual('boxever-flows-service', extract_repo_slug(scm_url))


if __name__ == '__main__':
    unittest.main()
