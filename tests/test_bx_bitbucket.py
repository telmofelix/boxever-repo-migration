
from unittest import TestCase

from analyzer.bx_bitbucket import BoxeverBitbucket, get_bitbucket_server_instance


class TestBoxeverBitbucket(TestCase):
    def test_load_repo_detail(self):
        repo = BoxeverBitbucket.load_repo_detail('boxever-ansible')
        print(repo.created_on[:10])
        print(repo.updated_on[:10])
        print(repo.slug)
        print(repo.scm)
        print(repo.size)
        print(repo.owner)
        print(repo.mainbranch)
        print(repo.__dict__.keys())
        print(repo.project['key'])
        print(repo.project['name'])

    def test_fetch_repo_detail(self):
        repo = BoxeverBitbucket(get_bitbucket_server_instance()).repo_by_full_name('boxever/boxever-ansible')
        for attr in repo.attributes():
            print(repo.__getattribute__(attr))
        print(repo.created_on[:10])
        print(repo.updated_on[:10])
        print(repo.slug)
        print(repo.scm)


    def test_fetch_repos(self):
        bx_bitbucket = BoxeverBitbucket(get_bitbucket_server_instance())
        admin_repos = bx_bitbucket.repos_by_owner_and_role("admin")
        member_repos = bx_bitbucket.repos_by_owner_and_role("member")

        print(sum(1 for _ in member_repos))

    def test_output_csv_for_all(self):
        slugs = BoxeverBitbucket.get_all_slugs()
        print(slugs)
